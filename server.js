var http = require("http");
var fs = require("fs");
var socketio = require("socket.io");
var contentTypes = {js: 'application/javascript', css: 'text/css', png: 'image/png', gif: "image/gif", html: 'text/html', glb: "model/gltf.binary"}
var mongoClient = require('mongodb').MongoClient
var ObjectID = require('mongodb').ObjectID
var games = []
var players = {}
var waiting;
let db;
let bricks = ["00","01","02","03","04","05","06","11","12","13","14","15","16","22","23","24","25","26","33","34","35","36","44","45","46","55","56","66"]

var server = http.createServer(function(req,res){
    var file = "static" + ((req.url == "/") ? "/index.html" : req.url)
    fs.readFile(file, function (error, data) {
        if(!error) {
            res.writeHead(200, { 'Content-Type': contentTypes[file.split(".").pop()]});
            res.write(data);
        }
        else {
            res.writeHead(404, { 'Content-Type': 'text/html' });            
            res.write("<h1>404 - Not found</h1>");
        }
        res.end();
    });
})

var io = socketio.listen(server) 


io.sockets.on("connection", function (client) {    
    client.on('disconnect', function() {
        if(waiting == client.id) waiting = undefined
        if(players[client.id]) {
            for(let id of players[client.id].players) if(id != client.id) io.sockets.to(id).emit("quit", {}), players[id] = undefined;
            delete players[client.id]
            players[client.id] = undefined;
        }
    });
    client.on("start", function () {
        if(!waiting) io.sockets.to(client.id).emit("wait", {}), waiting = client.id
        else {
            let game = {pool: shuffle(bricks.slice()), players: [waiting, client.id]}
            let plr1 = [], plr2 = [], target = true;
            for(let i = 0; i < 14; ++i) {
                (target ? plr1 : plr2).push(game.pool.pop())
                target = !target;
            }
            let double = "-1", normal = "00", final;
            for(let num of ["66","55","44","33","22","11","00"]) if(plr1.indexOf(num) > -1) {double = num; break}
            for(let num of ["66","55","44","33","22","11","00"]) if(plr2.indexOf(num) > -1 && parseInt(num) > parseInt(double)) {double = num; break}
            for(let num of plr1) if(num[0] != num[1] && parseInt(num[0]) + parseInt(num[1]) > parseInt(normal[0]) + parseInt(normal[1])) normal = num;
            for(let num of plr2) if(num[0] != num[1] && parseInt(num[0]) + parseInt(num[1]) > parseInt(normal[0]) + parseInt(normal[1])) normal = num;
            if(double != "-1") final = double;
            else final = normal
            io.sockets.to(waiting).emit("ready", {opponent: 1, dominoes: plr1, avilable: (plr1.indexOf(final) > -1) ? final : false, f: final});
            io.sockets.to(client.id).emit("ready", {opponent: 0, dominoes: plr2, avilable: (plr2.indexOf(final) > -1) ? final : false, f: final});
            players[waiting] = game
            players[client.id] = game
            games.push(game)
            waiting = undefined;
        }
    })
    client.on("move", function (data) {
        io.sockets.to(players[client.id].players[data.target]).emit("opponentMove", {id: data.num, ind: data.ind});
    })
    client.on("giveDomino", function () {
        if(players[client.id].pool.length) io.sockets.to(client.id).emit("granted", {id: players[client.id].pool.pop()});
        else io.sockets.to(client.id).emit("denied", {});
    })
    client.on("endGame", function (data) {
        io.sockets.to(players[client.id].players[data.target]).emit("lost", {});
    })
    client.on("scoreAsk", function (data) {
        io.sockets.to(players[client.id].players[data.target]).emit("scoreAsk", {});
    })
    client.on("scoreSent", function (data) {
        io.sockets.to(players[client.id].players[data.target]).emit("scoreSent", {score: data.score});
    })
    client.on("blockaded", function (data) {
        io.sockets.to(players[client.id].players[data.target]).emit("blockaded", {score: data.score});
    })
    client.on("winGame", function (data) {
        io.sockets.to(players[client.id].players[data.target]).emit("win", {score: data.score});
    })
    client.on("sendScore", function (data) {
	try {
		db.insertOne({score: data.s, nickname: data.n})
	}
	catch(e) {}
    })
    client.on("showScores", function (data) {
	try {
		db.find({}).toArray(function (err, items) {
			io.sockets.to(client.id).emit("highscores", {d: items.sort((a,b)=>{return parseInt(b.score) - parseInt(a.score)})});
         	});
	}
	catch(e) {
		io.sockets.to(client.id).emit("highscores", {d: []});
	}
    })

})


server.listen(3000, function(){});

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

process.argv.forEach(function (val, index, array) {
    if(index == 2) {
        mongoClient.connect("mongodb://" + array[index], function (err, cl) {
        	db = cl.db("game").collection("highscores")
        })
    }
});


