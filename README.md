# Block Dominoes

A classic "Block Dominoes" game written using three.js, socket.io and node.js. The rules can be found here: https://en.wikipedia.org/wiki/Dominoes#Blocking_game. It supports multiple sessions (each 2 players only), automating pairing (unfortunately I didn't implemented a lobby list) and a scoreboard.

## Prerequisites

To run the game You obviously need node.js installed on your system and a few npm packages. To install them use the npm command below. If You want the scoreboard to work You also need to install mongodb and start it (this is optional thus I'm not including any intructions here - You can easily find them online). 

```
npm install socket.io mongodb
```

After You've done that You need to start the game server (it starts an http server on port 3000). You need to cd into server.js's directory and then execute the command below - You can substitute "[mongodb server address]" for the address of your mongdb server but if You don't have one simply omit the second argument.

```
node server.js [mongodb server address]
```

## How to play

To play the game You need a browser and preferably somone to play it with you. You just type the address of the hosting machine in your browser plus the port (e.g. localhost:3000) and you are good to go. By clicking the play button the server automatically assigns You to a new lobby (and You wait) or an existing one if someone clicked it before You and You are the first person to click it after him (You are automatically matched with him - the game begins immediately) - the number of such lobbies is unlimited. The game is turn based so it alterates between you and your opponent. During your turn there are only 2 avilable in-game actions that you can take:
* Placing a tile - by selecting it from the menu on the left (or bottom - depending on the orientation of the screen) (grey ones wouldn't make an allowed move) and placing it on the board by clicking one of the tiles (allowed spots) with a green border.
* Moving your camera - by holding the right mouse button and moving the mouse.





