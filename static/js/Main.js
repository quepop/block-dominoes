let renderer = new THREE.WebGLRenderer({ antialias: true });
let scene = new THREE.Scene();
let camera = new THREE.PerspectiveCamera(60,window.innerWidth/window.innerHeight,0.1,20000);
let light = new THREE.PointLight( 0xffffff,1,800);
let controls
let raycaster = new THREE.Raycaster();
let avilable = [], potential = [], dominoes = {}, intersection;
let plane = new THREE.Mesh(new THREE.CubeGeometry(4000,10,4000),  new THREE.MeshPhongMaterial({
    aoMap: new THREE.TextureLoader().load("/gfx/floor/ao.png",repeat(16,16)),
    bumpMap: new THREE.TextureLoader().load("/gfx/floor/bump.png",repeat(16,16)),
    map: new THREE.TextureLoader().load("/gfx/floor/map.png",repeat(16,16)),
    normalMap: new THREE.TextureLoader().load("/gfx/floor/normal.png",repeat(16,16)),
    specularMap: new THREE.TextureLoader().load("/gfx/floor/gloss.png",repeat(16,16)),
    side: THREE.DoubleSide
 }));
(new THREE.GLTFLoader()).load("/gfx/domino/domino.glb",(gltf)=>{
    for(let obj of gltf.scene.children) {
        obj.scale.multiplyScalar(12)
        obj.position.set(0,0,0)
        obj.position.y = 5+(7.191073005535152/2)+0.2
        dominoes[obj.name] = obj;
    }
})

renderer.setClearColor(0);
camera.position.set(0,300,0)
camera.position.applyAxisAngle(new THREE.Vector3(1,0,0),0.6)
camera.lookAt(scene.position)
scene.add(light,plane);

$(document).ready(function () {
    $("body").append(renderer.domElement)
    window.onresize();
    worker();
})

function worker() {
    let V = camera.position.clone().sub((new THREE.Vector3(0,300,0)).applyAxisAngle(new THREE.Vector3(1,0,0),0.6))
    light.position.set(V.x,camera.position.y,V.z);
    renderer.render(scene, camera);
    window.requestAnimationFrame(worker);
}

function repeat(x,y) {
    return (texture) => {
        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
        texture.repeat.set(x,y);
    }
}

function drawAvilable(id,check = false) {
    for(let object of avilable) scene.remove(object,object.userData.wireframe);
    avilable = [];
    let counter = 0
    for(let pos of potential) {
        if(id[0] != pos.id && id[1] != pos.id) continue
        let domino = dominoes[id].clone(); 
        if(id[0] == id[1]) {
            domino.position.add(pos.cPos.clone().add((new THREE.Vector3(0,0,(-47.924030195364395/2)-12)).applyAxisAngle(new THREE.Vector3(0,1,0),-(Math.PI/2)*pos.direction)))
            if(!(pos.direction%2)) domino.rotateOnWorldAxis (new THREE.Vector3(0,1,0),-Math.PI/2) 
        }
        else {
            if(pos.short) domino.position.add(pos.cPos.clone().add((new THREE.Vector3(0,0,(-47.924030195364395/2)-12)).applyAxisAngle(new THREE.Vector3(0,1,0),-(Math.PI/2)*pos.direction)))
            else domino.position.add(pos.cPos.clone().add((new THREE.Vector3(0,0,-47.924030195364395)).applyAxisAngle(new THREE.Vector3(0,1,0),-(Math.PI/2)*pos.direction)))
            domino.rotateOnWorldAxis(new THREE.Vector3(0,1,0),-(Math.PI/2)*pos.direction) 
            if(pos.id == id[1]) domino.rotateOnWorldAxis(new THREE.Vector3(0,1,0),-Math.PI) 
        }
        domino.userData.pos = pos;
        domino.userData.wireframe = new THREE.BoxHelper( domino, 0x00ff00 );
        let box = (domino.userData.box = (new THREE.Box3()).setFromObject(domino))
        box.expandByScalar(-0.1)
        let intersects = false;
        for(let obj of scene.children) if(obj.type == "Group" && domino.userData.box.intersectsBox(obj.userData.box)) intersects = true;
        if(intersects) continue
        else ++counter
        if(check) continue
        domino.userData.index = avilable.length
        avilable.push(domino);
        scene.add(domino,domino.userData.wireframe);
    }
    return counter
}

window.onresize =  () => {
    renderer.setSize(window.innerWidth, window.innerHeight);
    camera.aspect = window.innerWidth/window.innerHeight;
    camera.updateProjectionMatrix();
}

window.onmousemove =  (e) => {
    raycaster.setFromCamera(new THREE.Vector2((e.clientX / window.innerWidth) * 2 - 1, -(e.clientY / window.innerHeight) * 2 + 1), camera)
    intersection = raycaster.intersectObjects(avilable,true)
    if(intersection.length) intersection[0].object.parent.userData.wireframe.material.color.setHex( 0xff0000 );
    else for(let mesh of avilable) mesh.userData.wireframe.material.color.setHex( 0x00ff00 );
}

window.onclick = (e,his) => {
    if(his == undefined && !intersection[0]) return
    let current = (his != undefined ? avilable[his] : intersection[0].object.parent)
    if(his == undefined) moveMade(current.name,current.userData.index)
    scene.remove(current.userData.wireframe); 
    for(let object of avilable) if(object != current) scene.remove(object,object.userData.wireframe);
    current.userData.pos.cPos = current.position.clone()
    current.userData.pos.cPos.y = 0;
    if(current.name[0] == current.name[1]) {
        if(potential.length == 1) potential.push({ cPos: current.userData.pos.cPos.clone(), direction: 3, id: current.name[0], short: true})
        current.userData.pos.short = true;
        for(let num of shiftDirections(current.userData.pos.direction)) potential.push({ cPos: current.userData.pos.cPos.clone(), direction: num, id: current.name[0], short: false})
    }
    else {
        if(potential.length == 1) potential.push({ cPos: current.userData.pos.cPos.clone(), direction: 3, id: current.userData.pos.id, short: false})
        current.userData.pos.short = false 
        current.userData.pos.id = (current.userData.pos.id == current.name[0] ? current.name[1] : current.name[0])
    }
    intersection[0] = undefined
    avilable = [];
    setAvilable();
}


function shiftDirections(num) {
    let directions = [3,0,1,2]
    for(let i = 0; i < num; ++i) directions.push(directions.shift())
    return directions.splice(0,3).filter((n)=>{return n!=num})
}  

function enableControls() {
    controls = Object.assign(new THREE.OrbitControls(camera),{enableZoom: false, enableRotate: false, enableKeys: false})    
}

