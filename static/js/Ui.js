$(document).ready(function () {
    startingScreen();
    helpMenu()
    $( "#h" ).hover(
        function() {
          $("#help").attr("style","");
        }, function() {
            $("#help").attr("style","display: none;");
        }
      );
})

function addDomino(id) {
    let img = $("<img>")
    $(img).attr("src","/gfx/domino/"+id+".png");
    $("#holder").append(img);
    $(img).on("click",()=>{
        if($(img).attr("class") !=  "avilable") return
        $("#selected").attr("id","")
        $(img).attr("id","selected")
        drawAvilable($(img).attr("src").split("/")[3].split(".")[0])
    })
}

function setAvilable() {
    $(".avilable").attr('class','')
    for(let pos of potential) {
        $("img[src$='"+pos.id+".png']").attr('class','avilable')
        $("img[src^='/gfx/domino/"+pos.id+"']").attr('class','avilable')
    }
    $(".avilable").each(function() {
        if(!drawAvilable($(this).attr("src").split("/")[3].split(".")[0],true)) $(this).attr("class","")
    });
}

function addIfNotAvilable() {
    if(!$(".avilable").length) askForDomino();
}

function startingScreen() {
    $("#protect").attr("style","")
    $("#protect").html("")
    let s = $("<div>")
    let b = $("<button>")
    let b1 = $("<button>")
    $(s).attr("id","s")
    $("#protect").append(s)
    $(s).append(b)
    $(s).append(b1)
    $(b).html("Play")
    $(b).attr("style","position: absolute; left: 50%; transform: translate(-50%, 0); top: 10%;")
    $(b).on("click",()=>{
        askForStart();
        waitMenu();
    })
    $(b1).html("Highscores")
    $(b1).attr("style","position: absolute; left: 50%; transform: translate(-50%, 0); bottom: 10%;")
    $(b1).on("click",()=>{
        askForHighscores();
    })
    
}

function queryHighscore(score) {
    communicate("YOU WON<br>YOUR SCORE IS:<br><a>"+score+"</a><br>ENTER YOUR NICKNAME BELOW TO SAVE IT")
    $("#protect").attr("style","")
    let s = $("<div>")
    let inp = $("<input>")
    let b = $("<button>")
    $(s).attr("id","s")
    $("#protect").append(s)
    $(s).append(b)
    $(s).append(inp)
    $(inp).attr("type","text")
    $(inp).attr("style","position: absolute; left: 50%; transform: translate(-50%, 0); top: 25%;")
    $(b).html("Save")
    $(b).on("click",()=>{ 
        if($(inp).val() == "") alert("Enter nickname")
        else sendScore(score,$(inp).val()), communicate("SAVED");
    })
    $(b).attr("style","position: absolute; left: 50%; transform: translate(-50%, 0); bottom: 25%;")
    controls.enabled = false;
}

function hideProtect() {
    $("#protect").attr("style","display: none;");
}

function communicate(text) {
    $("#protect").attr("style","")
    $("#protect").html(text)
}

function scoreSum() {
    let sum = 0
    $("img").each(function() {
        let num = $(this).attr("src").split("/")[3].split(".")[0]
        sum += parseInt(num[0]) + parseInt(num[1])
    });
    return sum
}

function drawHighscores(array){
    communicate("")
    let d = $("<div>")
    $(d).attr("id","d")
    let t = $("<table>")
    $(t).html("<tr><th>Nickname</th><th>Score</th></tr>")
    for(let obj of array) {
        let tr = $("<tr>")
        let td1 = $("<td>")
        let td2 = $("<td>")
        $(td1).html(obj.nickname)
        $(td2).html(obj.score)
        $(tr).append(td1)
        $(tr).append(td2)
        $(t).append(tr)
    }
    helpScores();
    $(d).append(t)
    $("#protect").append(d)
};

function helpGame() {
    $("#help").html("Gra w domino postepuje według tradycyjnych zasad. Aby wygrać musisz pozbyć się wszystkich kostek ze swojej reki lub w przypadku gdy wszyscy gracze sa zablokowani musisz miec w sumie jak najmniej oczek. Dostepne domina do polozenia na stole sa jasniejsze niz reszta, po wybraniu domina z panelu na planszy pojajwiaja potencjalne mozliwe miejsca w jakich klocek moze sie znalezc. Aby zatwierdzic ruch wystarczy kliknac w jedno z potencjalnych miejsc. Mozesz poruszać sie po mapie poruszajac myszka i przytrzymujac prawy klawisz myszy")
}

function helpMenu (){
    $("#help").html("Klikajac Play rozpoczynasz nową sesje lub dolaczasz do istniejacej jesli jakis gracz czeka na lobby, informacje na temat wynikow szukaj po wejsciu w highscores")
}

function waitMenu (){
    $("#help").html("Obecnie oczekujesz na graczy")
}

function helpScores(){
    $("#help").html("Tutaj widnieja wszystkie wyniki z wygranych gier łącznie z nikami graczy ktorzy je zdobyli")
}

