let client = io();
let opponent

function askForStart() {
    client.emit("start",{})
}

function askForDomino() {
    client.emit("giveDomino",{})
}

client.on("wait",()=>{
    communicate("Waiting for player to join...")
})

client.on("granted",(data)=>{
    addDomino(data.id);
    setAvilable();
    addIfNotAvilable()
})

client.on("denied",(data)=>{
    communicate("Opponent's turn...")
    client.emit("blockaded",{target: opponent, score: scoreSum()})
})

client.on("blockaded",(data)=>{
    hideProtect();
    if(!$(".avilable").length) {
        if(data.score > scoreSum()) client.emit("endGame",{target: opponent}), queryHighscore(data.score-scoreSum())
        else client.emit("winGame",{target: opponent, score: scoreSum()-data.score}), communicate("YOU LOST")
    }
})

client.on("lost",(data)=>{
    communicate("YOU LOST")
})

client.on("win",(data)=>{
    queryHighscore(data.score)
})

client.on("scoreAsk",(data)=>{
    client.emit("scoreSent",{target: opponent, score: scoreSum()})
})

client.on("scoreSent",(data)=>{
    queryHighscore(data.score)
})

client.on("quit",(data)=>{
    if(!$("#s").length && $("#protect").html() != "YOU LOST") communicate("Opponent disconnected")
})

client.on("highscores",(data)=>{
    drawHighscores(data.d);
})

client.on("ready",(data)=>{
    helpGame();
    for(let id of data.dominoes) addDomino(id);
    opponent = data.opponent;
    potential.push({cPos: new THREE.Vector3((-47.924030195364395/2)-12,0,0), direction: 1, id: data.f[0], short: true})
    if(data.avilable) {
        $("img[src='/gfx/domino/"+data.avilable+".png']").attr('class','avilable')
        hideProtect()
    }
    else communicate("Opponent's turn...")
    enableControls()
})

client.on("opponentMove",(data)=>{
    hideProtect()
    drawAvilable(data.id)
    window.onclick(null,data.ind);
    addIfNotAvilable()
})

function moveMade(id,index) {
    communicate("Opponent's turn...")
    $("#selected").remove();
    if(!$("img").length) client.emit("endGame",{target: opponent}), askForScore();
    else client.emit("move",{target: opponent, num: id, ind: index})
}

function askForScore() {
    client.emit("scoreAsk",{target: opponent})
}

function sendScore(score,nickname) {
    client.emit("sendScore",{n: nickname, s: score})
}

function askForHighscores() {
    client.emit("showScores",{})
}

